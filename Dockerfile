FROM quay.io/centos/centos:7
ARG internal_cert_rpm
ARG internal_rcm_repo
ARG gitbz_config_file
COPY get-pip.py /root/get-pip.py
RUN yum -y --nogpgcheck install "${internal_cert_rpm}" epel-release && \
    curl -L -o /etc/yum.repos.d/rcm.repo "${internal_rcm_repo}" && \
    yum -y install python-dist-git-utils && \
    yum -y install --disablerepo=rcm-tools-rhel-7-server-rpms python-requests GitPython && \
    touch /usr/lib/python2.7/site-packages/gitbz/__init__.py && \
    /root/get-pip.py && \
    pip install cachetools && \
    echo ${gitbz_config_file} > /etc/gitbz.json
COPY check_gitbz.py /usr/bin/check_gitbz
