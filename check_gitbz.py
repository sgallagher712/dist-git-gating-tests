#!/usr/bin/python

import json
import logging
import os
import re
import requests
import sys
from cachetools import cached, TTLCache
from shutil import rmtree
from tempfile import mkdtemp


import git as gitpython
import gitbz.utils as gitbz


# PROJECT_OVERRIDE is for components that do not match between CentOS Stream
# and RHEL dist-git, most notably the components that contained '+' characters
# in their names. We *MUST* be sure that distrobaker is configured to redirect
# the component syncs properly before adding an entry to this list

PROJECT_OVERRIDE = {
    "dvdplusrw-tools": "dvd+rw-tools",
    "centos-release": "redhat-release",
    "compat-sap-cplusplus-12": "compat-sap-c++-12",
    "libsigcplusplus20": "libsigc++20",
    "memtest86plus": "memtest86+",
    "perl-Text-TabsplusWrap": "perl-Text-Tabs+Wrap",
}


def check_gitbz(
    query_url, git_repo, namespace, project, oldrev, newrev, refname
):
    logger = logging.getLogger(__name__)
    repo_commits = gitbz.get_commits(git_repo, oldrev, newrev)
    commits_data = []
    fixed = []
    for repo_commit in repo_commits:
        speclines = gitbz.specAdditions(project, repo_commit)
        message = gitbz.get_commit_message(repo_commit)

        logger.debug(
            "gitbz data for %s: %s", repo_commit.hexsha, message + speclines
        )

        single_commit = {
            "hexsha": repo_commit.hexsha,
            "files": gitbz.get_commit_files(repo_commit),
            "resolved": gitbz.bugzillaIDs("Resolves?:", message + speclines),
            "related": gitbz.bugzillaIDs("Related?:", message + speclines),
            "reverted": gitbz.bugzillaIDs("Reverts?:", message + speclines),
        }
        fixed.extend(gitbz.bugzillaIDs("Fixes?:", message + speclines))
        commits_data.append(single_commit)

    request_data = {
        "package": project,
        "namespace": namespace,
        "ref": refname,
        "commits": commits_data,
    }

    logger.info("Sending data to gitbz API to verify hooks result ...")
    logger.debug(
        "Data sent: %s", json.dumps(request_data, sort_keys=True, indent=2)
    )

    res = requests.post(query_url, json=request_data, timeout=905)

    logger.debug("Response from gitbz API: %s", res.text)
    if not res.ok:
        logger.warning("Failed gitbz check for request: %s", request_data)

    payload = json.loads(res.text)
    if len(fixed) > 0:
        # These are bugs that were identified by the "Fixes:" prefix rather
        # than the "Resolves:" prefix.
        payload["logs"] += "\n"
        payload[
            "logs"
        ] += "*** WARNING: Detected one or more uses of 'Fixes:' instead of 'Resolves:'\n"
        payload["logs"] += "  Unverified:\n"
        for bug in fixed:
            payload["logs"] += "    {}\n".format(bug)

    return payload


def read_config(config_file):
    with open(config_file, "r") as f:
        c = json.load(f)
    return c


@cached(cache=TTLCache(maxsize=20, ttl=60 * 60))  # Cache for an hour
def determine_divergent_branch(rhel_version, api_url, namespace):
    logger = logging.getLogger(__name__)

    # Query the "package pages" API for the current active Y-stream release
    # Phase 230 is "Planning / Development / Testing" (AKA DeveTestDoc)
    request_params = {
        "phase": 230,
        "product__shortname": "rhel",
        "relgroup__shortname": rhel_version,
        "format": "json",
    }

    res = requests.get(
        os.path.join(api_url, "latest", "releases"),
        params=request_params,
        timeout=60,
    )
    res.raise_for_status()
    payload = json.loads(res.text)
    logger.debug(
        "Response from PP API: {}".format(json.dumps(payload, indent=2))
    )
    if len(payload) < 1:
        raise RuntimeError("Received zero potential release matches)")

    active_y_version = -1
    for entry in payload:
        shortname = entry["shortname"]

        # The shortname is in the form rhel-9-1.0
        # Extract the active Y-stream version
        m = re.search("(?<={}-)\d+(?=\.0)".format(rhel_version), shortname)
        if not m:
            raise RuntimeError(
                "Could not determine active Y-stream version from shortname"
            )
        y_version = int(m.group(0))
        if y_version > active_y_version:
            active_y_version = y_version

    # The divergent branch is Y-1
    return "{}.{}.0".format(rhel_version, active_y_version - 1)


def determine_refname(cfg, namespace, project, target_branch):
    logger = logging.getLogger(__name__)

    # Identify the RHELevant version
    try:
        rhel_version = cfg["mapping"][target_branch]
    except KeyError:
        # If it's not listed in the mapping, just return the target
        # branch. Most likely it is a stream branch.
        return target_branch

    # Look up the Y-1 branch name
    divergent_branch = determine_divergent_branch(
        rhel_version,
        cfg["api_url"],
        namespace,
    )
    logger.debug("Divergent branch: {}".format(divergent_branch))

    # The rhel-9.0.0 branch is a special case, it's unsupported
    if rhel_version == "rhel-9" and divergent_branch == "rhel-9.0.0":
        return target_branch

    # Determine if the Y-1 branch exists for this repo
    g = gitpython.cmd.Git()
    try:
        g.ls_remote(
            "--exit-code",
            os.path.join(cfg["rhel_dist_git"], namespace, project),
            divergent_branch,
        )
        branch_exists = True
    except gitpython.GitCommandError as e:
        t, v, tb = sys.exc_info()
        # `git ls-remote --exit-code` returns "2" if it cannot find the ref
        if e.status == 2:
            branch_exists = False
        else:
            raise

    # If the branch exists, then it means we've diverged and should use the
    # original Y-stream branch for gitbz checks
    if branch_exists:
        logger.info(
            "Branch {0} exists in RHEL dist-git. Using rules from {1}".format(
                divergent_branch, target_branch
            )
        )
        return target_branch
    else:
        # If it does not exist, then use the Y-1 branch for gitbz checks
        logger.info(
            "Branch {0} does not exist in RHEL dist-git. Using rules from {0}".format(
                divergent_branch
            )
        )
        return divergent_branch


def log_setup():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    # Log all messages into the debug log
    debug_handler = logging.FileHandler(
        "{}/debug.log".format(os.environ["CI_PROJECT_DIR"])
    )
    debug_handler.setLevel(logging.DEBUG)
    logger.addHandler(debug_handler)

    # Also log everything INFO and higher to the console
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger


def main():
    logger = log_setup()
    cfg = read_config("/etc/gitbz.json")

    logger.debug("Working Directory: {}".format(os.getcwd()))

    namespace = os.environ["CI_PROJECT_NAMESPACE"].rsplit("/")[-1]
    centos_git_repo = gitpython.Repo(path=os.environ["CI_PROJECT_DIR"])

    project = os.environ["CI_PROJECT_NAME"]
    if project in PROJECT_OVERRIDE:
        project = PROJECT_OVERRIDE[project]

    refname = determine_refname(
        cfg=cfg,
        namespace=namespace,
        project=project,
        target_branch=os.environ["CI_MERGE_REQUEST_TARGET_BRANCH_NAME"],
    )

    result = check_gitbz(
        query_url=cfg["gitbz_query_url"],
        git_repo=centos_git_repo,
        namespace=namespace,
        project=project,
        oldrev=os.environ["CI_MERGE_REQUEST_DIFF_BASE_SHA"],
        newrev=os.environ["CI_COMMIT_SHA"],
        refname="refs/heads/{}".format(refname),
    )

    logger.info(result["logs"])
    if result["result"] != "ok":
        logger.warning(result["error"])
        sys.exit(1)


if __name__ == "__main__":
    main()
